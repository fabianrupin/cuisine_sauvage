# Coding Style

.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
##
## Project setup
##---------------------------------------------------------------------------
cs: ## check cs problem
	./vendor/bin/php-cs-fixer fix --dry-run --stop-on-violation --diff

cs-fix: ## fix problems
	./vendor/bin/php-cs-fixer fix

cs-ci:
	./vendor/bin/php-cs-fixer fix src/ --dry-run --using-cache=no --verbose

fix: ## fix all
	./vendor/bin/php-cs-fixer fix
	./vendor/bin/phpstan

db-migrate: ## make migrations and add fixtures
	symfony console doctrine:migrations:migrate -n

##
## start and stop server
##---------------------------------------------------------------------------


start: ## start symfony server and watch dev
	docker compose up -d
	sleep 5
	symfony run -d yarn encore dev --watch
	symfony serve -d
	symfony console doctrine:database:drop --env=dev --force --if-exists
	symfony console doctrine:database:create --env=dev
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n --env=dev
	symfony proxy:start
	symfony proxy:domain:attach cuisinesauvage
	symfony open:local

db:
	docker compose up
	sleep 5
	symfony console doctrine:database:drop --env=dev --force --if-exists
	symfony console doctrine:database:create --env=dev
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n --env=dev

stop: ## stop server
	symfony server:stop
	docker compose down

start-preprod: ## start symfony server and gulp dev
	yarn build
	symfony console doctrine:database:drop --env=dev --force --if-exists
	symfony console doctrine:database:create --env=dev
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n --env=dev
	symfony serve --no-tls -d


##
## testing
##---------------------------------------------------------------------------
db-test-migrate: ## Create test database, loading test data and launch test
	symfony console doctrine:database:drop --env=test --force --if-exists
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
fixtures: ## Loading test data
	symfony console doctrine:database:drop --env=test --force --if-exists
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load -n --env=test
test: ## Launching PHPUnit tests
	symfony console doctrine:database:drop --env=test --force --if-exists
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load -n --env=test
	./bin/phpunit

deploy:
	git pull
	composer install --no-dev --optimize-autoloader
	yarn install
	yarn build
	symfony console c:c