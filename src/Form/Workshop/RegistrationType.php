<?php

namespace App\Form\Workshop;

use App\Entity\Registration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'form.firstname.label',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'form.lastname.label',
            ])
            ->add('street', TextType::class, [
                'label' => 'form.street.label',
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'form.zip_code.label',
            ])
            ->add('city', TextType::class, [
                'label' => 'form.city.label',
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'form.phone_number.label',
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email.label',
            ])
            ->add('participantsNumber', NumberType::class, [
                'label' => 'form.participants_number.label',
            ])
            ->add('howDidYouFind', ChoiceType::class, [
                'choices' => [
                    'Site internet' => 'Site internet',
                    'Réseaux sociaux' => 'Réseaux sociaux',
                    'Bouche à oreille' => 'Bouche à oreille',
                    'Bon cadeau (Merci de mettre le numéro de bon cadeau en commentaire)' => 'Bon cadeau',
                ],
                'label' => 'form.how_did_you_find.label',
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'form.comment.label',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Registration::class,
            'translation_domain' => 'registration',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
