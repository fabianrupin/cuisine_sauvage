<?php

namespace App\Controller;

use App\Entity\Registration;
use App\Service\StripePayment;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Stripe\StripeClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Workflow\WorkflowInterface;
use Webmozart\Assert\Assert;

class PaymentController extends AbstractController
{

    public function __construct(
        private readonly StripePayment $payment,
        private readonly EntityManagerInterface $entityManager,
        private readonly StripeClient $stripeClient,
        private readonly WorkflowInterface $paymentConfirmationWorkflow,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/payment/checkout/confirm', name: 'payment_confirm', methods: 'POST')]
    public function paymentConfirm(Request $request): JsonResponse
    {
        $paymentConfirmContent = json_decode($request->getContent());

        if ($paymentConfirmContent->type !== 'checkout.session.completed') {
            $this->logger->error('Received a non completed checkout', $paymentConfirmContent);

            return new JsonResponse(['message' => 'Not a completed checkout object'], 200);
        }

        $registrationNumber = $paymentConfirmContent->data->object->client_reference_id;

        $registration = $this->entityManager->getRepository(Registration::class)->findOneBy(
            ['uuid' => $registrationNumber]
        );

        if (!$registration) {
            $this->logger->error('Impossible to find registration with this number', $registrationNumber);

            return new JsonResponse(['message' => 'Unknown client reference id'], 404);
        }

        if (!$this->paymentConfirmationWorkflow->can(
                $registration,
                'payment_completed'
            ) && !$this->paymentConfirmationWorkflow->can($registration, 'full_payment')) {
            $this->logger->error(
                'Impossible apply a full payment status, or a complete payment status',
                $registrationNumber
            );

            return new JsonResponse(['message' => 'Registration status not compatible with this information'], 200);
        }

        if ($this->paymentConfirmationWorkflow->can($registration, 'payment_completed')) {
            $this->paymentConfirmationWorkflow->apply($registration, 'payment_completed');

            $this->logger->info('Payment completed', $registrationNumber);
        }

        if ($this->paymentConfirmationWorkflow->can($registration, 'full_payment')) {
            $this->paymentConfirmationWorkflow->apply($registration, 'full_payment');

            $this->logger->info('Payment full', $registrationNumber);
        }

        $this->entityManager->flush();

        return new JsonResponse(['message' => "Everything's goods"], 200);
    }

    #[Route('/inscription/{uuid}/payment', name: 'payment', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        Registration $registration,
        #[MapQueryParameter] ?string $success,
        #[MapQueryParameter] ?string $successCheckId
    ): Response {
        if ($success && $registration->getCardPaymentId() === $successCheckId) {
            $this->addFlash('success', 'payment.in_progress');

            $this->logger->info('Registered payment', ['registration' => $registration->getUuid()]);

            return $this->redirectToRoute('registration_show', ['uuid' => $registration->getUuid()]);
        }

        if ($registration->getRemainingAmount() <= 0) {
            $this->addFlash('warning', 'payment.already_done');

            $this->logger->info('Payment already done', [$registration->getUuid()]);

            return $this->redirectToRoute('registration_show', [
                'uuid' => $registration->getUuid(),
            ]);
        }

        if ('POST' === $request->getMethod()) {
            $successCheckId = Uuid::v4();

            $checkoutSession = $this->stripeClient->checkout->sessions->create([
                'line_items' => [
                    [
                        'price_data' => [
                            'currency' => 'eur',
                            'product_data' => [
                                'name' => $registration->getWorkshop()->getName()
                            ],
                            'unit_amount' => $registration->getRemainingAmount(),
                        ],
                        'quantity' => 1,
                    ],
                    [
                        'price_data' => [
                            'currency' => 'eur',
                            'product_data' => [
                                'name' => 'Frais de transaction'
                            ],
                            'unit_amount' => 200,
                        ],
                        'quantity' => $registration->getParticipantsNumber(),
                    ]
                ],
                'client_reference_id' => $registration->getUuid(),
                'customer_email' => $registration->getEmail(),
                'mode' => 'payment',
                'success_url' => $this->generateUrl(
                    'payment',
                    [
                        'uuid' => $registration->getUuid(),
                        'success' => 'true',
                        'successCheckId' => $successCheckId->toBase58()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'cancel_url' => $this->generateUrl(
                    'payment',
                    ['uuid' => $registration->getUuid()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ]);


            $registration->setPaymentMethod('card');
            $registration->setCardPaymentId($successCheckId->toBase58());
            $this->entityManager->flush();

            Assert::notNull($checkoutSession->url, 'Checkout Session URL is null.');
            return $this->redirect($checkoutSession->url);
        }

        $intent = $this->payment->paymentIntent($registration);

        return $this->render('payment/new.html.twig', [
            'registration' => $registration,
            'clientSecret' => $intent['client_secret'],
            'total' => $registration->getPrice() + $registration->getParticipantsNumber() * 200,
        ]);
    }
}
