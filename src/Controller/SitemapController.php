<?php

namespace App\Controller;

use App\Repository\WorkshopRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    #[Route('/sitemap.xml', name: 'sitemap', defaults: ['_format' => 'xml'])]
    public function index(WorkshopRepository $workshopRepository): Response
    {
        $hostname = $_ENV['SITE_BASE_URL'];

        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('homepage')];
        $urls[] = ['loc' => $this->generateUrl('biography')];
        $urls[] = ['loc' => $this->generateUrl('workshop_index')];
        $urls[] = ['loc' => $this->generateUrl('workshop_cooking_show')];
        $urls[] = ['loc' => $this->generateUrl('workshop_walking_show')];
        $urls[] = ['loc' => $this->generateUrl('partners')];
        $urls[] = ['loc' => $this->generateUrl('contact')];

        foreach ($workshopRepository->findAllNext() as $workshop) {
            $urls[] = [
                'loc' => $this->generateUrl('workshop_show', ['id' => $workshop->getId()]),
            ];
        }
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname,
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
