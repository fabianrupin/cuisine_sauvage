<?php

namespace App\Controller;

use App\Entity\Registration;
use App\Entity\Workshop;
use App\Form\Workshop\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\WorkflowInterface;

#[Route('/inscription')]
class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly WorkflowInterface $availabilityConfirmationWorkflow,
        private readonly WorkflowInterface $paymentConfirmationWorkflow,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/nouvelle/atelier/{id}', name: 'registration_new', methods: ['GET', 'POST'])]
    public function new(Request $request, Workshop $workshop): Response
    {
        if ($workshop->getRemainingPlaces() <= 0) {
            $this->addFlash('danger', 'registration_form.fail');

            return $this->redirectToRoute('workshop_index');
        }
        $registration = new Registration();
        $registration->setWorkshop($workshop);
        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registration->setPrice($registration->getWorkshop()->getPrice() * $registration->getParticipantsNumber());

            $this->entityManager->persist($registration);
            $this->entityManager->flush();

            try {
                $this->availabilityConfirmationWorkflow->apply($registration, 'to_availability_pending');
                $this->paymentConfirmationWorkflow->apply($registration, 'to_payment_create');
            } catch (\LogicException $exception) {
                $this->logger->error($exception->getMessage());
            }

            $this->entityManager->flush();

            $this->addFlash('success', 'registration_form.success');

            return $this->redirectToRoute('registration_show', ['uuid' => $registration->getUuid()]);
        }

        return $this->render('workshop/registration/new.html.twig', [
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{uuid}', name: 'registration_show', methods: ['GET'])]
    public function show(
        Registration $registration,
    ): Response {
        return $this->render('workshop/registration/show.html.twig', [
            'registration' => $registration,
        ]);
    }

    #[Route('/{uuid}/edit', name: 'registration_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Registration $registration): Response
    {
        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('workshop_index');
        }

        return $this->render('workshop/registration/edit.html.twig', [
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{uuid}/cancel', name: 'registration_cancel', methods: ['GET'])]
    public function cancel(Registration $registration): Response
    {
        try {
            $this->availabilityConfirmationWorkflow->apply($registration, 'cancel_request');
        } catch (\LogicException $e) {
            $this->logger->debug($e->getMessage());
        }

        try {
            $this->availabilityConfirmationWorkflow->apply($registration, 'cancel_registration_user');
        } catch (\LogicException $e) {
            $this->logger->debug($e->getMessage());
        }

        $this->entityManager->flush();

        return $this->redirectToRoute('workshop_index');
    }
}
