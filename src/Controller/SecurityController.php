<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserLoginType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Endroid\QrCode\Builder\Builder;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Totp\TotpAuthenticatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends BaseController
{
    #[Route('/login', name: 'app_login', methods: ['POST', 'GET'])]
    public function login(AuthenticationUtils $authenticationUtils, #[CurrentUser] ?User $user): Response
    {
        $userLoginForm = $this->createForm(UserLoginType::class);

        return $this->render('security/login.html.twig', [
            'user_login_form' => $userLoginForm->createView(),
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): never
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route('/authentication/2fa/enable', name: 'app_2fa_enable', methods: ['GET'])]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function enable2fa(TotpAuthenticatorInterface $totpAuthenticator, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        if (!$user->isTotpAuthenticationEnabled()) {
            $user->setTotpSecret($totpAuthenticator->generateSecret());

            $entityManager->flush();
        }

        return $this->render('security/enable2fa.html.twig');
    }

    #[Route('/authentication/2fa/disable', name: 'app_2fa_disable', methods: ['GET'])]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function disable2fa(EntityManagerInterface $entityManager, AdminUrlGenerator $adminUrlGenerator): Response
    {
        $user = $this->getUser();
        if ($user->isTotpAuthenticationEnabled()) {
            $user->setTotpSecret(null);

            $entityManager->flush();
        }

        return $this->redirect($adminUrlGenerator->generateUrl());
    }

    #[Route('/authentication/2fa/qr-code', name: 'app_qr_code', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function authenticatorQrCode(TotpAuthenticatorInterface $totpAuthenticator): Response
    {
        $qrCodeContent = $totpAuthenticator->getQRContent($this->getUser());
        $result = Builder::create()
            ->data($qrCodeContent)
            ->build();

        return new Response($result->getString(), 200, ['Content-Type' => 'image/png']);
    }
}
