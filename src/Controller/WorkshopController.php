<?php

namespace App\Controller;

use App\Entity\Workshop;
use App\Repository\WorkshopCategoryRepository;
use App\Repository\WorkshopRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/ateliers')]
class WorkshopController extends AbstractController
{
    final public const MAX_OCCURRENCE_PER_PAGE = 12;

    #[Route('/calendrier', name: 'workshop_index', methods: ['GET'])]
    public function index(Request $request, PaginatorInterface $paginator, WorkshopRepository $workshopRepository): Response
    {
        $searchTerm = $request->get('search');
        $queryBuilder = $workshopRepository->getWithSearchQueryBuilder($searchTerm);

        $workshops = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            self::MAX_OCCURRENCE_PER_PAGE
        );

        if ($request->query->get('preview')) {
            return $this->render('workshop/_searchPreview.html.twig', [
                'workshops' => $workshops,
            ]);
        }

        return $this->render('workshop/index.html.twig', [
            'workshops' => $workshops,
        ]);
    }

    #[Route('/{id}', name: 'workshop_show', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function show(Workshop $workshop): Response
    {
        return $this->render('workshop/show.html.twig', [
            'workshop' => $workshop,
        ]);
    }

    #[Route('/cueillette-et-cuisine-sauvage', name: 'workshop_cooking_show', methods: ['GET'])]
    public function cookingShow(WorkshopCategoryRepository $workshopCategoryRepository): Response
    {
        return $this->render('workshop/cooking-show.html.twig', [
            'cookingWorkshopCategory' => $workshopCategoryRepository->find(1),
        ]);
    }

    #[Route('/balade-decouverte-et-degustation', name: 'workshop_walking_show', methods: ['GET'])]
    public function walkingShow(WorkshopCategoryRepository $workshopCategoryRepository): Response
    {
        return $this->render('workshop/walking-show.html.twig', [
            'walkingWorkshopCategory' => $workshopCategoryRepository->find(2),
        ]);
    }
}
