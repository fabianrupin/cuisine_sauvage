<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartnersController extends AbstractController
{
    #[Route('/partners', name: 'partners', methods: ['GET'])]
    public function show(): Response
    {
        return $this->render('partners/index.html.twig', [
        ]);
    }
}
