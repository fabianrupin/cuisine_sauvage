<?php

namespace App\Controller;

use App\Service\LocationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/address')]
class AddressController extends AbstractController
{
    #[Route('/search', name: 'address', methods: ['GET'])]
    public function index(Request $request, LocationService $locationService): Response
    {
        $searchAddressTerm = $request->get('search');

        if ('' == $searchAddressTerm) {
            return $this->render('address/_searchAddressPreview.html.twig');
        }

        $result = $locationService->getExistingAddress($searchAddressTerm);

        return $this->render('address/_searchAddressPreview.html.twig', [
            'features' => $result->features,
        ]);
    }
}
