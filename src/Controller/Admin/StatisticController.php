<?php

namespace App\Controller\Admin;

use App\Manager\RegistrationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatisticController extends AbstractController
{
    #[Route('/admin/statistic', name: 'statistic', methods: ['GET'])]
    public function index(RegistrationManager $registrationManager): Response
    {
        return $this->render('admin/statistic/index.html.twig', [
            'registrationsChart' => $registrationManager->registrationsByMonthGraph(),
            'revenueChart' => $registrationManager->revenueByMonthGraph(),
        ]);
    }
}
