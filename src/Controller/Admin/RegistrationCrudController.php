<?php

namespace App\Controller\Admin;

use App\Config\AvailabilityStatus;
use App\Config\PaymentStatus;
use App\Entity\Registration;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly WorkflowInterface $availabilityConfirmationWorkflow,
        private readonly WorkflowInterface $paymentConfirmationWorkflow,
        private readonly AdminUrlGenerator $adminUrlGenerator,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Registration::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['workshop.startAt' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        yield DateTimeField::new('createdAt', $this->translator->trans('created_at', [], 'registration'))
            ->hideOnForm();
        yield TextField::new('UUID', $this->translator->trans('uuid', [], 'registration'))
            ->onlyOnDetail();
        yield TextField::new('fullName', $this->translator->trans('lastname', [], 'registration'));
        yield TextField::new('street', $this->translator->trans('street', [], 'registration'))
            ->hideOnIndex();
        yield TextField::new('zipCode', $this->translator->trans('zip_code', [], 'registration'))
            ->hideOnIndex();
        yield TextField::new('city', $this->translator->trans('city', [], 'registration'));
        yield TextField::new('phoneNumber', $this->translator->trans('phone_number', [], 'registration'));
        yield EmailField::new('email', $this->translator->trans('email', [], 'registration'))
            ->hideOnIndex();
        yield AssociationField::new('workshop', $this->translator->trans('workshop', [], 'registration'));
        yield ChoiceField::new('readableAvailabilityStatus', $this->translator->trans('availability_status', [], 'registration'))
            ->setChoices(AvailabilityStatus::statusType())
            ->renderAsBadges(AvailabilityStatus::badgeColor())
            ->hideOnForm();
        yield ChoiceField::new('readablePaymentStatus', $this->translator->trans('payment_status', [], 'registration'))
            ->setChoices(PaymentStatus::statusType())
            ->renderAsBadges(PaymentStatus::badgeColor())
            ->hideOnForm();
        yield NumberField::new('participantsNumber', $this->translator->trans('participants_number', [], 'registration'));
        yield MoneyField::new('price', $this->translator->trans('price', [], 'registration'))
            ->setCurrency('EUR');
        yield MoneyField::new('amountReceived', $this->translator->trans('amount_received', [], 'registration'))
            ->setCurrency('EUR');
        yield TextField::new('howDidYouFind', $this->translator->trans('how_did_you_find', [], 'registration'))
            ->onlyOnDetail();
        yield TextField::new('comment', $this->translator->trans('comment', [], 'registration'))
            ->onlyOnDetail();
        yield TextField::new('card_payment_id', $this->translator->trans('payment_details.id', [], 'registration'))
            ->onlyOnDetail();
    }

    public function configureActions(Actions $actions): Actions
    {
        $confirm = Action::new('confirm', 'registration.availability_confirm')
            ->linkToCrudAction('confirmAvailability')
            ->displayIf(fn (Registration $registration) => $this->availabilityConfirmationWorkflow->can($registration, 'confirm_availability')
                && ($registration->getWorkshop()->getEndsAt() > new DateTime()))
            ->addCssClass('btn btn-success');

        $deny = Action::new('deny', 'registration.availability_deny')
            ->linkToCrudAction('denyAvailability')
            ->displayIf(fn (Registration $registration) => $this->availabilityConfirmationWorkflow->can($registration, 'deny_availability')
                && ($registration->getWorkshop()->getEndsAt() > new DateTime()))
            ->addCssClass('btn btn-danger');

        $reverseDeny = Action::new('reverse_deny', 'registration.confirm')
            ->linkToCrudAction('reverseDeny')
            ->displayIf(fn (Registration $registration) => $this->availabilityConfirmationWorkflow->can($registration, 'reverse_deny')
                && ($registration->getWorkshop()->getEndsAt() > new DateTime()))
            ->addCssClass('btn');

        $cancel = Action::new('cancel', 'registration.cancel')
            ->linkToCrudAction('cancelRegistrationAdmin')
            ->displayIf(fn (Registration $registration) => $this->availabilityConfirmationWorkflow->can($registration, 'cancel_registration_admin')
                && ($registration->getWorkshop()->getEndsAt() > new DateTime()))
            ->addCssClass('btn');

        $confirmCompletePayment = Action::new('confirmCompletePayment', 'registration.payment_full')
            ->linkToCrudAction('confirmCompletePayment')
            ->displayIf(fn (Registration $registration) => $this->paymentConfirmationWorkflow->can($registration, 'payment_completed')
                && ($registration->getWorkshop()->getEndsAt() > new DateTime()))
            ->addCssClass('btn');

        $confirmFullPayment = Action::new('confirmFullPayment', 'registration.payment_full')
            ->linkToCrudAction('confirmFullPayment')
            ->displayIf(fn (Registration $registration) => $this->paymentConfirmationWorkflow->can($registration, 'full_payment'))
            ->addCssClass('btn');

        return $actions
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_DETAIL, $deny)
            ->add(Crud::PAGE_DETAIL, $confirm)
            ->add(Crud::PAGE_DETAIL, $reverseDeny)
            ->add(Crud::PAGE_DETAIL, $cancel)
            ->add(Crud::PAGE_DETAIL, $confirmCompletePayment)
            ->add(Crud::PAGE_DETAIL, $confirmFullPayment)
            ->add(Crud::PAGE_INDEX, $deny)
            ->add(Crud::PAGE_INDEX, $confirm)
            ->add(Crud::PAGE_INDEX, $reverseDeny)
            ->add(Crud::PAGE_INDEX, $cancel)
            ->add(Crud::PAGE_INDEX, $confirmCompletePayment)
            ->add(Crud::PAGE_INDEX, $confirmFullPayment);
    }

    public function confirmAvailability(AdminContext $context): Response
    {
        return $this->move($context, $this->availabilityConfirmationWorkflow, 'confirm_availability');
    }

    private function move(AdminContext $context, WorkflowInterface $workflow, string $transition): Response
    {
        $entity = $context->getEntity()->getInstance();

        $this->adminUrlGenerator
            ->setController(self::class)
            ->setAction('index')
            ->removeReferrer()
            ->setEntityId(null);

        try {
            $workflow->apply($entity, $transition);
            $this->entityManager->flush();
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirect($this->adminUrlGenerator->generateUrl());
    }

    public function denyAvailability(AdminContext $context): Response
    {
        return $this->move($context, $this->availabilityConfirmationWorkflow, 'deny_availability');
    }

    public function reverseDeny(AdminContext $context): Response
    {
        return $this->move($context, $this->availabilityConfirmationWorkflow, 'reverse_deny');
    }

    public function cancelRegistrationAdmin(AdminContext $context): Response
    {
        return $this->move($context, $this->availabilityConfirmationWorkflow, 'cancel_registration_admin');
    }

    public function confirmCompletePayment(AdminContext $context): Response
    {
        return $this->move($context, $this->paymentConfirmationWorkflow, 'payment_completed');
    }

    public function confirmFullPayment(AdminContext $context): Response
    {
        return $this->move($context, $this->paymentConfirmationWorkflow, 'full_payment');
    }
}
