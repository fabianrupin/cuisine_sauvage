<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use App\Entity\Registration;
use App\Entity\Workshop;
use App\Entity\WorkshopCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $routeBuilder = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(WorkshopCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTranslationDomain('admin')
            ->setTitle('Administration Cuisine Sauvage');
    }

    public function configureAssets(): Assets
    {
        return parent::configureAssets()
            ->addWebpackEncoreEntry('admin');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->addMenuItems([
                MenuItem::linkToRoute('user_form.edit.title', 'fas fa-user-edit', 'user_edit'),
            ]);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('dashboard.back_homepage', 'fa fa-home', 'homepage');
        yield MenuItem::linkToCrud('dashboard.workshops', 'fas fa-calendar-alt', Workshop::class);
        yield MenuItem::linkToCrud('dashboard.registrations', 'fas fa-user-alt', Registration::class);
        yield MenuItem::linkToCrud('dashboard.workshop_categories', 'fas fa-chalkboard-teacher', WorkshopCategory::class);
        yield MenuItem::linkToCrud('dashboard.addresses', 'fas fa-map-marked-alt', Address::class);
        yield MenuItem::linktoRoute('dashboard.statistic', 'fas fa-chart-line', 'statistic');
    }
}
