<?php

namespace App\Controller\Admin;

use App\Entity\WorkshopCategory;
use App\Form\WorkshopCategoryType;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/categorie-ateliers')]
class WorkshopCategoryController extends AbstractController
{
    public function __construct(private readonly UploaderHelper $uploaderHelper, private readonly EntityManagerInterface $manager)
    {
    }

    #[Route('/{id}/modifier', name: 'workshop_category_picture_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, WorkshopCategory $workshopCategory): Response
    {
        $form = $this->createForm(WorkshopCategoryType::class, $workshopCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ToDo : adding a DTO to process file
            if ($uploadedFile = $form['picture']->getData()) {
                $pictureFilename = $this->uploaderHelper->uploadWorkshopCategoryPicture(
                    $uploadedFile,
                    $workshopCategory->getPictureFilename()
                );

                $workshopCategory->setPictureFilename($pictureFilename);
            }

            $this->manager->flush();
        }

        return $this->render('admin/workshop_category/edit.html.twig', [
            'workshopCategory' => $workshopCategory,
            'form' => $form->createView(),
        ]);
    }
}
