<?php

namespace App\Controller\Admin;

use App\Entity\WorkshopCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class WorkshopCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WorkshopCategory::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'workshop_category.name'),
            TextEditorField::new('description', 'workshop_category.description'),
            TextEditorField::new('practicalInformation', 'workshop_category.practical_information'),
            TimeField::new('indicativeStartAt', 'workshop_category.start_at'),
            TimeField::new('indicativeEndsAt', 'workshop_category.ends_at'),
            NumberField::new('indicativeCapacity', 'workshop_category.capacity'),
            MoneyField::new('indicativePrice', 'workshop_category.price')->setCurrency('EUR'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $picture = Action::new('picture', 'workshop_category.picture.edit', 'fa fa-image')
            ->linkToRoute('workshop_category_picture_edit', fn (WorkshopCategory $workshopCategory) => [
                'id' => $workshopCategory->getId(),
                'method' => 'GET',
            ]);

        return $actions
            ->add(Crud::PAGE_EDIT, $picture);
    }
}
