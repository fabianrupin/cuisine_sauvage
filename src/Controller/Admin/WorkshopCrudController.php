<?php

namespace App\Controller\Admin;

use App\Entity\Workshop;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class WorkshopCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Workshop::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['startAt' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'workshop.name'),
            TextEditorField::new('description', 'workshop.description'),
            DateTimeField::new('startAt', 'workshop.start_at'),
            DateTimeField::new('endsAt', 'workshop.ends_at'),
            NumberField::new('capacity', 'workshop.capacity'),
            NumberField::new('reservedRegistrations', 'workshop.reserved_registrations')->hideOnIndex(),
            NumberField::new('remainingPlaces', 'workshop.remaining_places')->hideOnForm(),
            MoneyField::new('price', 'workshop.price')->setCurrency('EUR'),
            AssociationField::new('workshopCategory', 'workshop.workshop_category'),
            AssociationField::new('address', 'workshop.address'),
            AssociationField::new('registrations', 'workshop.registrations')->hideOnForm(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $picture = Action::new('picture', 'workshop.picture.edit', 'fa fa-image')
            ->linkToRoute('workshop_picture_edit', fn (Workshop $workshop) => [
                'id' => $workshop->getId(),
                'method' => 'GET',
            ]);
        $duplicate = Action::new('duplicate', 'workshop.duplicate')
            ->linkToRoute('workshop_duplicate', fn (Workshop $workshop) => [
                'id' => $workshop->getId(),
                'method' => 'GET',
            ]);
        $participantsList = Action::new('participantsList', 'workshop.registrations')
            ->linkToRoute('workshop_participants', fn (Workshop $workshop) => [
                'id' => $workshop->getId(),
                'method' => 'GET',
            ]);

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, $picture)
            ->add(Crud::PAGE_INDEX, $duplicate)
            ->add(Crud::PAGE_DETAIL, $participantsList);
    }
}
