<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AddressCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Address::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('streetNumber', 'address.street_number'),
            TextField::new('route', 'address.route'),
            TextField::new('postalCode', 'address.postal_code'),
            TextField::new('locality', 'address.locality'),
        ];
    }
}
