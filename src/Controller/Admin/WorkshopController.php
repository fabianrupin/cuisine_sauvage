<?php

namespace App\Controller\Admin;

use App\Entity\Workshop;
use App\Form\Workshop\WorkshopDateType;
use App\Form\Workshop\WorkshopPictureType;
use App\Repository\RegistrationRepository;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/ateliers')]
class WorkshopController extends AbstractController
{
    public function __construct(
        private readonly UploaderHelper $uploaderHelper,
        private readonly EntityManagerInterface $manager,
        private readonly RegistrationRepository $registrationRepository)
    {
    }

    #[Route('/{id}/modifier', name: 'workshop_picture_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Workshop $workshop): Response
    {
        $form = $this->createForm(WorkshopPictureType::class, $workshop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ToDo: Create DTO to process file
            if ($uploadedFile = $form['picture']->getData()) {
                $pictureFilename = $this->uploaderHelper->uploadWorkshopPicture($uploadedFile, $workshop->getPictureFilename());
                $workshop->setPictureFilename($pictureFilename);
            }

            $this->manager->flush();

            return $this->redirectToRoute('workshop_index');
        }

        return $this->render('admin/workshop/edit.html.twig', [
            'workshop' => $workshop,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/dupliquer', name: 'workshop_duplicate', methods: ['GET', 'POST'])]
    public function duplicate(Request $request, Workshop $workshop): Response
    {
        $duplicateWorkshop = Workshop::duplicate($workshop);

        $form = $this->createForm(WorkshopDateType::class, $duplicateWorkshop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($duplicateWorkshop);
            $this->manager->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/workshop/duplicate.html.twig', [
            'workshop' => $workshop,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/participants', name: 'workshop_participants', methods: ['GET'])]
    public function listWorkshopParticipants(Workshop $workshop): Response
    {
        $participants = $this->registrationRepository->findBy(['workshop' => $workshop]);

        return $this->render('admin/workshop/participants.html.twig', [
            'participants' => $participants,
            'workshop' => $workshop,
        ]);
    }
}
