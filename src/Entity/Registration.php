<?php

namespace App\Entity;

use App\Config\AvailabilityStatus;
use App\Config\PaymentStatus;
use App\Repository\RegistrationRepository;
use App\Validator\Constraints as CustomAssert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RegistrationRepository::class)]
class Registration
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $firstname = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $lastname = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $street = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $zipCode = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $city = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[CustomAssert\FrenchMobilePhoneNumber()]
    private ?string $phoneNumber = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Email]
    private ?string $email = null;

    #[ORM\Column(type: 'array')]
    private array $availabilityStatus = [];

    #[ORM\ManyToOne(targetEntity: Workshop::class, inversedBy: 'registrations')]
    #[ORM\JoinColumn(nullable: false)]
    private Workshop $workshop;

    #[ORM\Column(type: 'uuid', unique: true)]
    private UuidV4 $uuid;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $cardPaymentId = null;

    #[ORM\Column(type: 'array')]
    private array $paymentStatus = [];

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $paymentMethod = null;

    #[ORM\Column(type: 'integer')]
    private ?int $price = null;

    #[ORM\Column(type: 'integer')]
    #[Assert\Expression('this.getParticipantsNumber() <= this.getWorkshop().getRemainingPlaces(this.getParticipantsNumber())',
        message: 'registration.not_enough_available_places')]
    private int $participantsNumber = 1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $howDidYouFind = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $comment = null;

    #[ORM\Column(type: 'integer')]
    private int $amountReceived = 0;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
    }

    public function __toString(): string
    {
        return "$this->firstname $this->lastname - $this->uuid";
    }

    public function getFullName(): string
    {
        return "$this->firstname $this->lastname";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getVerifiedEmail(): string
    {
        return $this->email ?: 'unknow';
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWorkshop(): Workshop
    {
        return $this->workshop;
    }

    public function setWorkshop(Workshop $workshop): self
    {
        $this->workshop = $workshop;

        return $this;
    }

    public function getAvailabilityStatus(): ?array
    {
        return $this->availabilityStatus;
    }

    public function setAvailabilityStatus(array $availabilityStatus): self
    {
        $this->availabilityStatus = $availabilityStatus;

        return $this;
    }

    public function getReadableAvailabilityStatus(): string
    {
        return (string) array_key_first($this->availabilityStatus);
    }

    public function getVerboseAvailabilityStatus(): string
    {
        return AvailabilityStatus::from($this->getReadableAvailabilityStatus())->frenchStatus();
    }

    public function getUuid(): UuidV4
    {
        return $this->uuid;
    }

    public function setUuid(UuidV4 $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getCardPaymentId(): ?string
    {
        return $this->cardPaymentId;
    }

    public function setCardPaymentId(?string $cardPaymentId): self
    {
        $this->cardPaymentId = $cardPaymentId;

        return $this;
    }

    public function getPaymentStatus(): ?array
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(array $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getReadablePaymentStatus(): string
    {
        return (string) array_key_first($this->paymentStatus);
    }

    public function getVerbosePaymentStatus(): string
    {
        return PaymentStatus::from($this->getReadablePaymentStatus())->frenchStatus();
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getParticipantsNumber(): int
    {
        return $this->participantsNumber;
    }

    public function setParticipantsNumber(int $participantsNumber): void
    {
        $this->participantsNumber = $participantsNumber;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getHowDidYouFind(): ?string
    {
        return $this->howDidYouFind;
    }

    public function setHowDidYouFind(?string $howDidYouFind): self
    {
        $this->howDidYouFind = $howDidYouFind;

        return $this;
    }

    public function getAmountReceived(): ?int
    {
        return $this->amountReceived;
    }

    public function setAmountReceived(int $amountReceived): self
    {
        $this->amountReceived = $amountReceived;

        return $this;
    }

    public function getRemainingAmount(): int
    {
        $remainingAmount = $this->price - $this->amountReceived;
        if ($remainingAmount < 0) {
            $remainingAmount = 0;
        }

        return $remainingAmount;
    }
}
