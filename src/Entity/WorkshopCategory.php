<?php

namespace App\Entity;

use App\Repository\WorkshopCategoryRepository;
use App\Service\UploaderHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkshopCategoryRepository::class)]
class WorkshopCategory implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $practicalInformation = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $pictureFilename = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $indicativeStartAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $indicativeEndsAt = null;

    #[ORM\Column(type: 'integer')]
    private ?int $indicativeCapacity = null;

    #[ORM\Column(type: 'integer')]
    private ?int $indicativePrice = null;

    #[ORM\OneToMany(mappedBy: 'workshopCategory', targetEntity: Workshop::class)]
    private Collection $workshops;

    public function __construct()
    {
        $this->workshops = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return (string) $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPracticalInformation(): string
    {
        return (string) $this->practicalInformation;
    }

    public function setPracticalInformation(?string $practicalInformation): void
    {
        $this->practicalInformation = $practicalInformation;
    }

    public function getPictureFilename(): ?string
    {
        return $this->pictureFilename;
    }

    public function getPicturePath(): string
    {
        return UploaderHelper::WORKSHOP_CATEGORY_PICTURE.'/'.$this->getPictureFilename();
    }

    public function setPictureFilename(?string $pictureFilename): void
    {
        $this->pictureFilename = $pictureFilename;
    }

    public function getIndicativeStartAt(): \DateTime
    {
        return $this->indicativeStartAt;
    }

    public function setIndicativeStartAt(\DateTime $indicativeStartAt): void
    {
        $this->indicativeStartAt = $indicativeStartAt;
    }

    public function getIndicativeEndsAt(): ?\DateTime
    {
        return $this->indicativeEndsAt;
    }

    public function setIndicativeEndsAt(\DateTime $indicativeEndsAt): void
    {
        $this->indicativeEndsAt = $indicativeEndsAt;
    }

    public function getIndicativeCapacity(): ?int
    {
        return $this->indicativeCapacity;
    }

    public function setIndicativeCapacity(int $indicativeCapacity): self
    {
        $this->indicativeCapacity = $indicativeCapacity;

        return $this;
    }

    public function getIndicativePrice(): ?int
    {
        return $this->indicativePrice;
    }

    public function setIndicativePrice($indicativePrice): void
    {
        $this->indicativePrice = $indicativePrice;
    }

    /**
     * @return Collection|Workshop[]
     */
    public function getWorkshops(): Collection
    {
        return $this->workshops;
    }

    public function addWorkshop(Workshop $workshop): self
    {
        if (!$this->workshops->contains($workshop)) {
            $this->workshops[] = $workshop;
            $workshop->setWorkshopCategory($this);
        }

        return $this;
    }

    public function removeWorkshop(Workshop $workshop): self
    {
        if ($this->workshops->removeElement($workshop)) {
            // set the owning side to null (unless already changed)
            if ($workshop->getWorkshopCategory() === $this) {
                $workshop->setWorkshopCategory(null);
            }
        }

        return $this;
    }
}
