<?php

namespace App\Entity;

use App\Repository\WorkshopRepository;
use App\Service\UploaderHelper;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: WorkshopRepository::class)]
class Workshop implements \Stringable
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $pictureFilename = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $startAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $endsAt = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $capacity = null;

    #[ORM\Column(type: 'integer')]
    private int $reservedRegistrations = 0;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $price = null;

    #[ORM\ManyToOne(targetEntity: WorkshopCategory::class, inversedBy: 'workshops')]
    #[ORM\JoinColumn(nullable: false)]
    private ?WorkshopCategory $workshopCategory = null;

    #[ORM\Column(type: 'array')]
    private array $status = [];

    #[ORM\ManyToOne(targetEntity: Address::class, inversedBy: 'workshops')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Address $address = null;

    /**
     * @ORM\OneToMany(targetEntity=Registration::class, mappedBy="workshop")
     */
    #[ORM\OneToMany(mappedBy: 'workshop', targetEntity: Registration::class)]
    private Collection $registrations;

    public function __construct()
    {
        $this->registrations = new ArrayCollection();
    }

    public function __toString(): string
    {
        if (is_null($this->startAt)) {
            return (string) $this->workshopCategory;
        }

        return $this->startAt->format('Y-m-d').' - '.$this->workshopCategory;
    }

    public static function duplicate(Workshop $workshop): Workshop
    {
        $duplicateWorkshop = new self();
        $duplicateWorkshop->name = $workshop->getName();
        $duplicateWorkshop->description = $workshop->getDescription();
        $duplicateWorkshop->pictureFilename = $workshop->getPictureFilename();
        $duplicateWorkshop->pictureFilename = $workshop->getPictureFilename();
        $duplicateWorkshop->startAt = $workshop->getStartAt();
        $duplicateWorkshop->endsAt = $workshop->getEndsAt();
        $duplicateWorkshop->capacity = $workshop->getCapacity();
        $duplicateWorkshop->price = $workshop->getPrice();
        $duplicateWorkshop->workshopCategory = $workshop->getWorkshopCategory();
        $duplicateWorkshop->address = $workshop->getAddress();

        return $duplicateWorkshop;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTime
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTime $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndsAt(): ?\DateTime
    {
        return $this->endsAt;
    }

    public function setEndsAt(\DateTime $endsAt): self
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    public function getDuration(): string
    {
        if ($this->startAt && $this->endsAt) {
            $carbonStartAt = CarbonImmutable::createFromMutable($this->startAt);
            $carbonEndsAt = CarbonImmutable::createFromMutable($this->endsAt);

            return $carbonEndsAt->locale('fr_FR')->diffForHumans($carbonStartAt, true, true, 2);
        }

        return 'Aucune durée calculable';
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return (string) $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPictureFilename(): ?string
    {
        return $this->pictureFilename;
    }

    public function getPicturePath(): string
    {
        return UploaderHelper::WORKSHOP_PICTURE.'/'.$this->getPictureFilename();
    }

    public function setPictureFilename(?string $pictureFilename): void
    {
        $this->pictureFilename = $pictureFilename;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): void
    {
        $this->capacity = $capacity;
    }

    public function getReservedRegistrations(): int
    {
        return $this->reservedRegistrations;
    }

    public function setReservedRegistrations(int $reservedRegistrations): void
    {
        $this->reservedRegistrations = $reservedRegistrations;
    }

    public function getRemainingPlaces($currentRegistrationNumber = null): ?int
    {
        $remainingPlaces = $this->capacity - $this->reservedRegistrations + $currentRegistrationNumber;

        foreach ($this->getRegistrations() as $registration) {
            if (
                array_key_exists('availability_confirmed', $registration->getAvailabilityStatus()) or
                array_key_exists('availability_pending', $registration->getAvailabilityStatus())
            ) {
                $remainingPlaces = $remainingPlaces - $registration->getParticipantsNumber();
            }
        }

        if ($remainingPlaces <= 0) {
            return 0;
        }

        return $remainingPlaces;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getWorkshopCategory(): ?WorkshopCategory
    {
        return $this->workshopCategory;
    }

    public function setWorkshopCategory(?WorkshopCategory $workshopCategory): self
    {
        $this->workshopCategory = $workshopCategory;

        return $this;
    }

    public function getStatus(): ?array
    {
        return $this->status;
    }

    public function setStatus(array $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations[] = $registration;
            $registration->setWorkshop($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getWorkshop() === $this) {
                $registration->setWorkshop(null);
            }
        }

        return $this;
    }
}
