<?php

namespace App\Repository;

use App\Entity\Registration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Registration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Registration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Registration[]    findAll()
 * @method Registration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Registration::class);
    }

    public function countRegistrationByMonth(): array
    {
        return $this->createQueryBuilder('registration')
            ->innerJoin('registration.workshop', 'workshop')
            ->andWhere('workshop.startAt < CURRENT_DATE()')
            ->select('count(registration) as nb_registrations, SUBSTRING(workshop.startAt, 1,7) as month')
            ->orderBy('workshop.startAt')
            ->groupBy('month')
            ->getQuery()
            ->getResult();
    }

    public function theoreticalRevenuePerMonth(): array
    {
        return $this->createQueryBuilder('registration')
            ->innerJoin('registration.workshop', 'workshop')
            ->andWhere('workshop.startAt < CURRENT_DATE()')
            ->select('SUM(registration.price) / 100 as nb_registrations, SUBSTRING(workshop.startAt, 1,7) as month')
            ->orderBy('workshop.startAt')
            ->groupBy('month')
            ->getQuery()
            ->getResult();
    }

    public function realRevenuePerMonth(): array
    {
        return $this->createQueryBuilder('registration')
            ->innerJoin('registration.workshop', 'workshop')
            ->andWhere('workshop.startAt < CURRENT_DATE()')
            ->select('SUM(registration.amountReceived) / 100 as nb_registrations, SUBSTRING(workshop.startAt, 1,7) as month')
            ->orderBy('workshop.startAt')
            ->groupBy('month')
            ->getQuery()
            ->getResult();
    }
}
