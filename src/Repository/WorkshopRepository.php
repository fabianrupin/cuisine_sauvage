<?php

namespace App\Repository;

use App\Entity\Workshop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Workshop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Workshop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Workshop[]    findAll()
 * @method Workshop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkshopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workshop::class);
    }

    public function getWithSearchQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->findAllQueryBuilder($term);

        return $qb
//            ->andWhere('occurrence.publishable = 1')
//            ->andWhere('occurrence.publishedAt < CURRENT_TIMESTAMP()')
            ->andWhere('workshop.endsAt > CURRENT_TIMESTAMP()')
            ->orderBy('workshop.startAt');
    }

    public function findAllQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('workshop')
            ->innerJoin('workshop.workshopCategory', 'workshop_category')
            ->innerJoin('workshop.address', 'address')
            ->addSelect('workshop');

        if ($term) {
            $qb->andWhere('workshop.name LIKE :term OR workshop_category.name LIKE :term OR address.route LIKE :term OR address.locality LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $qb;
    }

    /**
     * @return Workshop[] Returns an array of Workshop objects
     */
    public function findAllNext(): array
    {
        return $this->createQueryBuilder('workshop')
            ->andWhere('workshop.endsAt > CURRENT_TIMESTAMP()')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Workshop[] Returns an array of Workshop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Workshop
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
