<?php

namespace App\Config;

enum AvailabilityStatus: string
{
    case REQUEST = 'request';
    case PENDING = 'availability_pending';
    case DENIED = 'availability_denied';
    case CONFIRMED = 'availability_confirmed';
    case CANCELLED_BY_ADMIN = 'cancelled_by_admin';
    case CANCELLED_BY_USER = 'cancelled_by_user';

    public function frenchStatus(): string
    {
        return match ($this) {
            self::REQUEST => 'Requête faîte',
            self::PENDING => 'En attente',
            self::DENIED => 'Demande Refusée',
            self::CONFIRMED => 'Disponibilité Confirmée',
            self::CANCELLED_BY_ADMIN => "Annulé par l'administrateur",
            self::CANCELLED_BY_USER => "Annulé par l'utilisateur",
        };
    }

    public static function badgeColor(): array
    {
        return [
            self::REQUEST->value => 'primary',
            self::PENDING->value => 'warning',
            self::DENIED->value => 'danger',
            self::CONFIRMED->value => 'success',
            self::CANCELLED_BY_ADMIN->value => 'danger',
            self::CANCELLED_BY_USER->value => 'danger',
        ];
    }

    public static function statusType(): array
    {
        $statusType = [];
        foreach (self::cases() as $case) {
            $statusType[$case->frenchStatus()] = $case->value;
        }

        return $statusType;
    }
}
