<?php

namespace App\Config;

enum PaymentStatus: string
{
    case REQUEST = 'request';
    case CLOSED = 'closed';
    case NONE = 'none';
    case PARTIAL = 'partial';
    case COMPLETE = 'complete';
    case REFUND = 'refund';

    public function frenchStatus(): string
    {
        return match ($this) {
            self::REQUEST => 'Requête faîte',
            self::CLOSED => 'Paiement fermé',
            self::NONE => 'Aucun paiement',
            self::PARTIAL => 'Paiement partiel',
            self::COMPLETE => 'Paiement complet',
            self::REFUND => 'Remboursé',
        };
    }

    public static function badgeColor(): array
    {
        return [
            self::REQUEST->value => 'primary',
            self::CLOSED->value => 'primary',
            self::NONE->value => 'danger',
            self::PARTIAL->value => 'warning',
            self::COMPLETE->value => 'success',
            self::REFUND->value => 'secondary',
        ];
    }

    public static function statusType(): array
    {
        $statusType = [];
        foreach (self::cases() as $case) {
            $statusType[$case->frenchStatus()] = $case->value;
        }

        return $statusType;
    }
}
