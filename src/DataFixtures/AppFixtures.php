<?php

namespace App\DataFixtures;

use App\Factory\AddressFactory;
use App\Factory\RegistrationFactory;
use App\Factory\UserFactory;
use App\Factory\WorkshopCategoryFactory;
use App\Factory\WorkshopFactory;
use App\Service\UploaderHelper;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UploaderHelper $uploaderHelper)
    {
    }

    public function load(ObjectManager $manager): void
    {
        UserFactory::createOne([
            'email' => 'fabianrupin@hotmail.com',
            'isVerified' => true,
            'password' => 'needles',
            'roles' => ['ROLE_ADMIN'],
        ]);

        WorkshopCategoryFactory::createOne([
            'name' => 'Ateliers « Cueillette et cuisine sauvage »',
        ]);

        WorkshopCategoryFactory::createOne([
            'name' => 'Balade découverte et dégustation',
            'indicativeStartAt' => new DateTime('1970-01-01 10:30'),
            'indicativeEndsAt' => new DateTime('1970-01-01 12:30'),
            'pictureFilename' => $this->uploaderHelper->uploadWorkshopCategoryPicture(new File(__DIR__.'/images/walking-workshop.jpeg')),
        ]);

        AddressFactory::createOne([
            'route' => 'La Mettrie',
            'locality' => 'Bais',
            'postalCode' => '35680',
        ]);
        WorkshopFactory::createMany(20);
        RegistrationFactory::createMany(20);
    }
}
