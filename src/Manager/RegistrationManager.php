<?php

namespace App\Manager;

use App\Repository\RegistrationRepository;
use Carbon\CarbonImmutable;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class RegistrationManager
{
    public function __construct(
        private readonly RegistrationRepository $registrationRepository,
        private readonly ChartBuilderInterface $chartBuilder)
    {
    }

    public static function thisMonth(): CarbonImmutable|string
    {
        $thisMonth = new CarbonImmutable();

        return $thisMonth->locale('fr_FR');
    }

    public function registrationsByMonthGraph(): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => $this->getMonthTitleOnOneYear(),
            'datasets' => [
                [
                    'label' => 'Inscriptions',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $this->createDataByMonth($this->registrationRepository->countRegistrationByMonth()),
                ],
            ],
        ]);

        return $chart;
    }

    public function revenueByMonthGraph(): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => $this->getMonthTitleOnOneYear(),
            'datasets' => [
                [
                    'label' => 'Revenus théoriques',
                    'borderColor' => 'rgb(138, 191, 139)',
                    'data' => $this->createDataByMonth($this->registrationRepository->theoreticalRevenuePerMonth()),
                ],
                [
                    'label' => 'Revenus perçus',
                    'backgroundColor' => 'rgb(138, 191, 139)',
                    'borderColor' => 'rgb(138, 191, 139)',
                    'data' => $this->createDataByMonth($this->registrationRepository->realRevenuePerMonth()),
                ],
            ],
        ]);

        return $chart;
    }

    private function createDataByMonth(array $dataFromRepo): array
    {
        $data = [];

        for ($i = 11; $i >= 0; --$i) {
            $month = self::thisMonth()->subMonth();
            $isData = false;
            foreach ($dataFromRepo as $monthData) {
                if ($monthData['month'] === $month->isoFormat('Y-MM')) {
                    $data[] = $monthData['nb_registrations'];
                    $isData = true;
                }
            }
            if (false === $isData) {
                $data[] = 0;
            }
        }

        return $data;
    }

    private function getMonthTitleOnOneYear(): array
    {
        $thisMonth = new CarbonImmutable();

        $monthTitle = [];

        for ($i = 11; $i >= 0; --$i) {
            $monthTitle[] = ucfirst($thisMonth->subMonths($i)->locale('fr_Fr')->isoFormat('MMMM Y'));
        }

        return $monthTitle;
    }
}
