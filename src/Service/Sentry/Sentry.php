<?php

namespace App\Service\Sentry;

use Sentry\Event;
use Sentry\EventHint;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Sentry
{
    public function beforeSend(Event $event, EventHint $hint): ?Event
    {
        if ($hint->exception instanceof NotFoundHttpException) {
            return null;
        }

        if ($hint->exception instanceof MethodNotAllowedException) {
            return null;
        }

        return $event;
    }
}
