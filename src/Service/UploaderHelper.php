<?php

namespace App\Service;

use Exception;
use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    final public const WORKSHOP_PICTURE = 'workshop_picture';
    final public const WORKSHOP_CATEGORY_PICTURE = 'workshop_category_picture';

    public function __construct(
        private readonly Filesystem $publicUploadFilesystem,
        private readonly Filesystem $privateUploadFilesystem,
        private readonly RequestStackContext $requestStackContext,
        private readonly LoggerInterface $logger,
        private readonly string $publicAssetsBaseUrl)
    {
    }

    public function uploadWorkshopPicture(File $file, ?string $existingFilename): string
    {
        $destination = self::WORKSHOP_PICTURE;

        return $this->uploadPublicFile($file, $destination, $existingFilename);
    }

    public function uploadWorkshopCategoryPicture(File $file, ?string $existingFilename = null): string
    {
        $destination = self::WORKSHOP_CATEGORY_PICTURE;

        return $this->uploadPublicFile($file, $destination, $existingFilename);
    }

    private function uploadPublicFile(File $file, string $destination, ?string $existingFilename): string
    {
        $newFilename = $this->uploadFile($file, $destination, true);

        if ($existingFilename) {
            try {
                $this->publicUploadFilesystem->delete($destination.'/'.$existingFilename);
            } catch (FilesystemException $e) {
                $this->logger->alert(sprintf('Old uploaded file "%s" missing when trying to delete. '.$e->getMessage(), $existingFilename));
            }
        }

        return $newFilename;
    }

    /*    private function uploadPrivateFile(File $file, string $destination): string
        {
            return $this->uploadFile($file, $destination, false);
        }*/

    public function getPublicPath(string $path): string
    {
        return $this->requestStackContext->getBasePath().$this->publicAssetsBaseUrl.'/'.$path;
    }

    private function uploadFile(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $filesystem = $isPublic ? $this->publicUploadFilesystem : $this->privateUploadFilesystem;

        $newFilename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME)).'-'.uniqid().'.'.$file->guessExtension();

        $stream = fopen($file->getPathname(), 'r');

        try {
            $filesystem->writeStream(
                $directory.'/'.$newFilename,
                $stream
            );
        } catch (FilesystemException $e) {
            $this->logger->alert(sprintf('Could not write uploaded file"%s". '.$e->getMessage(), $newFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }

    /**
     * @return resource
     *
     * @throws Exception
     */
    public function readStream(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->publicUploadFilesystem : $this->privateUploadFilesystem;

        $resource = $filesystem->readStream($path);

        /*        if (false === $resource) {
                    throw new Exception(sprintf('Error opening stream for "%s"', $path));
                }*/

        return $resource;
    }
}
