<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LocationService
{
    public function __construct(private readonly HttpClientInterface $client, private readonly LoggerInterface $logger)
    {
    }

    public function getExistingAddress(string $address): mixed
    {
        try {
            $response = $this->client->request('GET', 'https://api-adresse.data.gouv.fr/search/', [
                'query' => [
                    'q' => $address,
                ],
            ]);

            return json_decode($response->getContent(), null, 512, JSON_THROW_ON_ERROR);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return [];
    }
}
