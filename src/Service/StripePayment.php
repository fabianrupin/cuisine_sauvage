<?php

namespace App\Service;

use App\Entity\Registration;
use Psr\Log\LoggerInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class StripePayment
{
    public function __construct(string $stripeSecretKey, private readonly LoggerInterface $logger)
    {
        Stripe::setApiKey($stripeSecretKey);
    }

    public function paymentIntent(Registration $registration): PaymentIntent
    {
        return PaymentIntent::create([
            'amount' => $registration->getRemainingAmount() + $registration->getParticipantsNumber() * 200,
            'currency' => 'eur',
            'payment_method_types' => ['card'],
        ]);
    }

    public function isPaymentValid(?string $cardPaymentId): bool
    {
        if (!$cardPaymentId) {
            return false;
        }

        try {
            $payment_intent = PaymentIntent::retrieve($cardPaymentId);
        } catch (ApiErrorException $e) {
            $this->logger->error($e->getMessage());
        }

        if (!isset($payment_intent) || !$payment_intent instanceof PaymentIntent) {
            return false;
        }

        if ('succeeded' != $payment_intent->status) {
            return false;
        }

        return true;
    }
}
