<?php

namespace App\Service\Mailer;

use App\Model\DTO\ContactFormDTO;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactMailer
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly TranslatorInterface $translator,
        private readonly string $emailAdmin,
        private readonly string $emailNameAdmin)
    {
    }

    public function sendContactMessage(ContactFormDTO $contactFormDTO): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject($this->translator->trans('contact.subject', [], 'email'))
            ->htmlTemplate('email/contact.html.twig')
            ->context(['contactFormDTO' => $contactFormDTO, 'emailTo' => $this->emailAdmin]);
        $this->mailer->send($email);

        return $email;
    }
}
