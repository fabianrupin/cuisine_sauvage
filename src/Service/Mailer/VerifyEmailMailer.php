<?php

namespace App\Service\Mailer;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class VerifyEmailMailer
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly TranslatorInterface $translator,
        private readonly VerifyEmailHelperInterface $verifyEmailHelper)
    {
    }

    public function sendVerifyEmail(User $user): TemplatedEmail
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            'app_verify_email',
            (string) $user->getId(),
            (string) $user->getEmail(), [
                'id' => $user->getId(),
            ]
        );

        $email = (new TemplatedEmail())
            ->to(new Address((string) $user->getEmail(), 'Administrateur CuisineSauvage'))
            ->subject($this->translator->trans('email_verify.subject', [], 'email'))
            ->htmlTemplate('email/verify_email.html.twig')
            ->context(['signedUrl' => $signatureComponents->getSignedUrl(), 'emailTo' => $user->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }
}
