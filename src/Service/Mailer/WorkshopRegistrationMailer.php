<?php

namespace App\Service\Mailer;

use App\Entity\Registration;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class WorkshopRegistrationMailer
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly TranslatorInterface $translator,
        private readonly string $emailAdmin,
        private readonly string $emailNameAdmin)
    {
    }

    public function sendAvailabilityPendingUserMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.pending.user.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/pending_user.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendAvailabilityPendingAdminMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject($this->translator->trans('workshop_registration.pending.admin.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/pending_admin.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $this->emailAdmin]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendAvailabilityConfirmationMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.confirmation.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/confirmation.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendAvailabilityDenialMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.denied.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/denial.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendCancelByAdminMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.cancel_by_admin.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/cancel_admin.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendCancelByUserMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject($this->translator->trans('workshop_registration.cancel_by_user.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/cancel_user.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $this->emailAdmin]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendPartialPaymentReceivedUserMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.partial_payment.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/partial_payment.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendFullPaymentReceivedUserMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address((string) $registration->getEmail(), $registration->getFirstname().' '.$registration->getLastname()))
            ->subject($this->translator->trans('workshop_registration.full_payment_user.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/full_payment_admin.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $registration->getEmail()]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendFullPaymentReceivedAdminMessage(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject($this->translator->trans('workshop_registration.full_payment_admin.subject', [], 'email'))
            ->htmlTemplate('email/workshop_registration/full_payment_admin.html.twig')
            ->context(['registration' => $registration, 'emailTo' => $this->emailAdmin]);
        $this->mailer->send($email);

        return $email;
    }
}
