<?php

namespace App\EventSubscriber;

use App\Entity\Workshop;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EAUploadPictureSubscriber implements EventSubscriberInterface
{
    public function onBeforeCrudActionEvent(BeforeEntityPersistedEvent $event): void
    {
        $workshop = $event->getEntityInstance();
        if (!$workshop instanceof Workshop) {
            return;
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['onBeforeCrudActionEvent'],
        ];
    }
}
