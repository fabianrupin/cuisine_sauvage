<?php

namespace App\EventSubscriber\EasyAdmin;

use App\Entity\Registration;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\WorkflowInterface;

class AmountReceivedSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkflowInterface $paymentConfirmationWorkflow)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityUpdatedEvent::class => ['setPaymentStatus'],
        ];
    }

    public function setPaymentStatus(BeforeEntityUpdatedEvent $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getEntityInstance();

        if (!(($registration) instanceof Registration)) {
            return;
        }

        if ($registration->getAmountReceived() <= 0) {
            return;
        }

        if ($registration->getRemainingAmount() <= 0) {
            if ($this->paymentConfirmationWorkflow->can($registration, 'full_payment')) {
                $this->paymentConfirmationWorkflow->apply($registration, 'full_payment');

                return;
            }
            $this->paymentConfirmationWorkflow->apply($registration, 'payment_completed');

            return;
        }

        try {
            $this->paymentConfirmationWorkflow->apply($registration, 'to_partial_payment');
        } catch (LogicException) {
        }
    }
}
