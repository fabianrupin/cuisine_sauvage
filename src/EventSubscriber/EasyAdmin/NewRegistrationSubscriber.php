<?php

namespace App\EventSubscriber\EasyAdmin;

use App\Entity\Registration;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class NewRegistrationSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkflowInterface $availabilityConfirmationWorkflow)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['setRegistrationStatus'],
        ];
    }

    public function setRegistrationStatus(BeforeEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Registration)) {
            return;
        }

        $this->availabilityConfirmationWorkflow->apply($entity, 'create_from_admin');
    }
}
