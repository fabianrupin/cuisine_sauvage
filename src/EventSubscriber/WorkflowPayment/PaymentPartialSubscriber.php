<?php

namespace App\EventSubscriber\WorkflowPayment;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class PaymentPartialSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer)
    {
    }

    public function onPartial(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        $this->mailer->sendPartialPaymentReceivedUserMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.payment_confirmation.entered.partial' => 'onPartial',
        ];
    }
}
