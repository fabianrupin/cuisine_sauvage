<?php

namespace App\EventSubscriber\WorkflowPayment;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class PaymentFullSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer)
    {
    }

    public function onComplete(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        $this->mailer->sendFullPaymentReceivedUserMessage($registration);
        $this->mailer->sendFullPaymentReceivedAdminMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.payment_confirmation.entered.complete' => 'onComplete',
        ];
    }
}
