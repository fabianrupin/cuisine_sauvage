<?php

namespace App\EventSubscriber\WorkflowAvailability;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class WorkshopRegistrationDenialSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer)
    {
    }

    public function onDenied(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        $this->mailer->sendAvailabilityDenialMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.availability_confirmation.entered.availability_denied' => 'onDenied',
        ];
    }
}
