<?php

namespace App\EventSubscriber\WorkflowAvailability;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class WorkshopRegistrationCancelledByAdminSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer)
    {
    }

    public function onCancelledByAdmin(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        $this->mailer->sendCancelByAdminMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.availability_confirmation.entered.cancelled_by_admin' => 'onCancelledByAdmin',
        ];
    }
}
