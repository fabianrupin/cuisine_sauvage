<?php

namespace App\EventSubscriber\WorkflowAvailability;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class WorkshopRegistrationPendingSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer)
    {
    }

    public function onPending(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        $this->mailer->sendAvailabilityPendingUserMessage($registration);
        $this->mailer->sendAvailabilityPendingAdminMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.availability_confirmation.entered.availability_pending' => 'onPending',
        ];
    }
}
