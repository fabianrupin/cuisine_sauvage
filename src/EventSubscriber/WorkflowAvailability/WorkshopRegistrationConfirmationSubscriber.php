<?php

namespace App\EventSubscriber\WorkflowAvailability;

use App\Entity\Registration;
use App\Service\Mailer\WorkshopRegistrationMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\WorkflowInterface;

class WorkshopRegistrationConfirmationSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly WorkshopRegistrationMailer $mailer, private readonly WorkflowInterface $paymentConfirmationWorkflow)
    {
    }

    public function onConfirmed(Event $event): void
    {
        /** @var Registration $registration */
        $registration = $event->getSubject();

        if ($this->paymentConfirmationWorkflow->can($registration, 'to_payment_create')) {
            $this->paymentConfirmationWorkflow->apply($registration, 'to_payment_create');
        }
        $this->paymentConfirmationWorkflow->apply($registration, 'to_payment_pending');

        $this->mailer->sendAvailabilityConfirmationMessage($registration);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.availability_confirmation.entered.availability_confirmed' => 'onConfirmed',
        ];
    }
}
