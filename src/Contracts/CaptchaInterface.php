<?php

namespace App\Contracts;

interface CaptchaInterface
{
    /**
     * @return array<string>
     */
    public function getChallenge(): array;

    public function checkAnswer(string $givenAnswer, string $expectedAnswer): bool;
}
