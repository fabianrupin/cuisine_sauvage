<?php

namespace App\Factory;

use App\Entity\Registration;
use App\Entity\Workshop;
use App\Repository\WorkshopRepository;
use Symfony\Component\Workflow\WorkflowInterface;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static         Workshop|Proxy createOne(array $attributes = [])
 * @method static         Workshop[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static         Workshop|Proxy findOrCreate(array $attributes)
 * @method static         Workshop|Proxy random(array $attributes = [])
 * @method static         Workshop|Proxy randomOrCreate(array $attributes = [])
 * @method static         Workshop[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static         Workshop[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static         WorkshopRepository|RepositoryProxy repository()
 * @method Workshop|Proxy create($attributes = [])
 */
final class RegistrationFactory extends ModelFactory
{
    public function __construct(
        private readonly WorkflowInterface $availabilityConfirmationWorkflow,
        private readonly WorkflowInterface $paymentConfirmationWorkflow)
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            'firstname' => self::faker()->firstName(),
            'lastname' => self::faker()->lastName(),
            'street' => self::faker()->streetAddress(),
            'zipcode' => self::faker()->postcode(),
            'city' => self::faker()->city(),
            'phoneNumber' => self::faker()->phoneNumber(),
            'email' => self::faker()->email(),
            'workshop' => WorkshopFactory::random(),
        ];
    }

    protected function initialize(): self
    {
        return $this
            ->afterInstantiate(function (Registration $registration) {
                $this->availabilityConfirmationWorkflow->apply($registration, 'to_availability_pending');
                $this->paymentConfirmationWorkflow->apply($registration, 'to_payment_create');
                $registration->setPrice($registration->getWorkshop()->getPrice());
            })
        ;
    }

    protected static function getClass(): string
    {
        return Registration::class;
    }
}
