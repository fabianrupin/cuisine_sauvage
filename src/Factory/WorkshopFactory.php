<?php

namespace App\Factory;

use App\Entity\Workshop;
use App\Repository\WorkshopRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static         Workshop|Proxy createOne(array $attributes = [])
 * @method static         Workshop[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static         Workshop|Proxy findOrCreate(array $attributes)
 * @method static         Workshop|Proxy random(array $attributes = [])
 * @method static         Workshop|Proxy randomOrCreate(array $attributes = [])
 * @method static         Workshop[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static         Workshop[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static         WorkshopRepository|RepositoryProxy repository()
 * @method Workshop|Proxy create($attributes = [])
 */
final class WorkshopFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            'name' => 'Plantes sauvages de fin d’hiver',
            'description' => "Ceci est le paragraphe pour dire que c'est un atelier de fin d'hiver",
            'workshopCategory' => WorkshopCategoryFactory::random(),
            'address' => AddressFactory::random(),
        ];
    }

    protected function initialize(): self
    {
        return $this
            ->afterInstantiate(function (Workshop $workshop) {
                $startAt = self::faker()->dateTimeBetween('-80 days', '+150 days');
                $endsAt = clone $startAt;

                if ('Ateliers « Cueillette et cuisine sauvage »' === $workshop->getWorkshopCategory()->getName()) {
                    $startAt->setTime(9, 30);
                    $endsAt->setTime(15, 30);
                } else {
                    $startAt->setTime(10, 00);
                    $endsAt->setTime(12, 00);
                }

                $workshop->setStartAt($startAt);
                $workshop->setEndsAt($endsAt);
                $workshop->setPrice($workshop->getWorkshopCategory()->getIndicativePrice());
                $workshop->setCapacity($workshop->getWorkshopCategory()->getIndicativeCapacity());
            })
        ;
    }

    protected static function getClass(): string
    {
        return Workshop::class;
    }
}
