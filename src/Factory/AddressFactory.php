<?php

namespace App\Factory;

use App\Entity\Address;
use App\Repository\AddressRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static        Address|Proxy createOne(array $attributes = [])
 * @method static        Address[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static        Address|Proxy findOrCreate(array $attributes)
 * @method static        Address|Proxy random(array $attributes = [])
 * @method static        Address|Proxy randomOrCreate(array $attributes = [])
 * @method static        Address[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static        Address[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static        AddressRepository|RepositoryProxy repository()
 * @method Address|Proxy create($attributes = [])
 */
final class AddressFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            'route' => self::faker()->streetAddress(),
            'locality' => self::faker()->city(),
            'postalCode' => self::faker()->postcode(),
        ];
    }

    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Address $address) {})
        ;
    }

    protected static function getClass(): string
    {
        return Address::class;
    }
}
