<?php

namespace App\Factory;

use App\Entity\WorkshopCategory;
use App\Repository\WorkshopCategoryRepository;
use App\Service\UploaderHelper;
use Symfony\Component\HttpFoundation\File\File;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<WorkshopCategory>
 *
 * @method static                 WorkshopCategory|Proxy createOne(array $attributes = [])
 * @method static                 WorkshopCategory[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static                 WorkshopCategory|Proxy find(object|array|mixed $criteria)
 * @method static                 WorkshopCategory|Proxy findOrCreate(array $attributes)
 * @method static                 WorkshopCategory|Proxy first(string $sortedField = 'id')
 * @method static                 WorkshopCategory|Proxy last(string $sortedField = 'id')
 * @method static                 WorkshopCategory|Proxy random(array $attributes = [])
 * @method static                 WorkshopCategory|Proxy randomOrCreate(array $attributes = [])
 * @method static                 WorkshopCategory[]|Proxy[] all()
 * @method static                 WorkshopCategory[]|Proxy[] findBy(array $attributes)
 * @method static                 WorkshopCategory[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static                 WorkshopCategory[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static                 WorkshopCategoryRepository|RepositoryProxy repository()
 * @method WorkshopCategory|Proxy create(array|callable $attributes = [])
 */
final class WorkshopCategoryFactory extends ModelFactory
{
    public function __construct(private readonly UploaderHelper $uploaderHelper)
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->text(70),
            'description' => self::faker()->text(),
            'practicalInformation' => self::faker()->text(),
            'indicativeStartAt' => new \DateTime('1970-01-01 09:30'),
            'indicativeEndsAt' => new \DateTime('1970-01-01 15:30'),
            'indicativeCapacity' => self::faker()->numberBetween(4, 25),
            'indicativePrice' => self::faker()->numberBetween(500, 8000),
            'pictureFilename' => $this->uploaderHelper->uploadWorkshopCategoryPicture(new File(__DIR__.'/../DataFixtures/images/cooking-workshop.jpeg')),
        ];
    }

    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(WorkshopCategory $workshopCategory): void {})
        ;
    }

    protected static function getClass(): string
    {
        return WorkshopCategory::class;
    }
}
