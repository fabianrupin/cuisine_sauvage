<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211122141045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration ADD participants_number INT DEFAULT NULL, ADD how_did_you_find VARCHAR(255) DEFAULT NULL, ADD comment LONGTEXT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('UPDATE registration SET participants_number = 1, created_at = NOW(), updated_at = NOW()');
        $this->addSql('ALTER TABLE workshop ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('UPDATE workshop SET created_at = NOW(), updated_at = NOW()');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration DROP participants_number, DROP how_did_you_find, DROP comment, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE workshop DROP created_at, DROP updated_at');
    }
}
