<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210907142832 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration ADD card_payment_id VARCHAR(255) DEFAULT NULL, ADD payment_status LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', ADD payment_method VARCHAR(255) DEFAULT NULL, ADD price INT NOT NULL, ADD amount_received INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration DROP card_payment_id, DROP payment_status, DROP payment_method, DROP price, DROP amount_received');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
