<?php

return [
    'APP_SECRET' => null,
    'DATABASE_URL' => null,
    'MAILER_DSN' => null,
    'SENTRY_DSN' => null,
    'STRIPE_PK' => null,
    'STRIPE_SK' => null,
];
