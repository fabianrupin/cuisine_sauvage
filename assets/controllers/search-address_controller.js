import {Controller} from "stimulus";
import {useClickOutside, useDebounce} from "stimulus-use";

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static values = {
        url: String,
    }

    static targets = ['result', 'street', 'postcode', 'city'];
    static debounces = ['search'];

    connect() {
        useClickOutside(this);
        useDebounce(this, { wait: 200 });
    }

    onClickAddress(event) {
        this.streetTarget.value = event.currentTarget.dataset.street
        this.postcodeTarget.value = event.currentTarget.dataset.postcode
        this.cityTarget.value = event.currentTarget.dataset.city
        this.resultTarget.innerHTML = '';
    }

    onSearchInput(event) {
        if (event.currentTarget.value !== '') {
            this.search(event.currentTarget.value);
        }
    }

    async search(query) {
        const params = new URLSearchParams({
            search: query,
        });
        const response = await fetch(`${this.urlValue}?${params.toString()}`);
        this.resultTarget.innerHTML = await response.text();
    }

    clickOutside(event) {
        this.resultTarget.innerHTML = '';
    }
}