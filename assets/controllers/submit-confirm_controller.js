import { Controller } from 'stimulus';
import Swal from "sweetalert2";
import { useDispatch} from "stimulus-use";

const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-primary',
    },
    buttonsStyling: false
})

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static values = {
        title: String,
        text: String,
        icon: String,
        confirmButton: String,
        submitAsync: Boolean
    }

    onSubmit(event) {
        event.preventDefault();

        swalWithBootstrapButtons.fire({
            title: this.titleValue || null,
            html: this.textValue || null,
            icon: this.iconValue || null,
            showCloseButton: true,
            confirmButtonText: this.confirmButtonValue || 'Oui',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return this.submitForm();
            }
        });
    }

    submitForm() {
        if(!this.submitAsyncValue) {
            this.element.submit();

            return;
        }
        return fetch(this.element.action, {
            method: this.element.method,
            body: new URLSearchParams(new FormData(this.element))
        });
    }
}