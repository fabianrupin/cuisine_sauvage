import {Controller} from 'stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    static values = {
        stripePublicKey: String,
        registrationUuid: String,
        clientSecret: String,
        cardHolderName: String,
        cardHolderEmail: String
    }

    connect() {

        const stripe = Stripe(this.stripePublicKeyValue);

        const client_secret = this.clientSecretValue;
        const cardHolderName = this.cardHolderNameValue;
        const cardHolderEmail = this.cardHolderEmailValue;
        const registrationUuid = this.registrationUuidValue;

        const elements = stripe.elements();
        const card = elements.create('card');

        card.mount('#card-elements');
        card.addEventListener('change', function (event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            }
        });
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            stripe
                .confirmCardPayment(client_secret, {
                    payment_method: {
                        card: card,
                        billing_details: {
                            name: cardHolderName,
                            email: cardHolderEmail
                        },
                        metadata: {
                            registration: registrationUuid
                        }
                    },
                })
                .then((result) => {
                if (result.error) {
                    console.log(result.error)
                } else if ('paymentIntent' in result) {
                    stripeTokenHandler(result.paymentIntent);
                }
            })
        });

        function stripeTokenHandler(intent) {
            var form = document.getElementById('payment-form');
            var InputIntentId = document.createElement('input');
            var InputAmount = document.createElement('input');
            var InputIntentStatus = document.createElement('input');
            var InputRegistration = document.createElement('input');
            InputIntentId.setAttribute('type', 'hidden');
            InputIntentId.setAttribute('name', 'stripeIntentId');
            InputIntentId.setAttribute('value', intent.id);
            InputAmount.setAttribute('type', 'hidden');
            InputAmount.setAttribute('name', 'amount');
            InputAmount.setAttribute('value', intent.amount);
            InputIntentStatus.setAttribute('type', 'hidden');
            InputIntentStatus.setAttribute('name', 'stripeIntentStatus');
            InputIntentStatus.setAttribute('value', intent.status);
            InputRegistration.setAttribute('type', 'hidden');
            InputRegistration.setAttribute('name', 'registration');
            InputRegistration.setAttribute('value', registrationUuid);
            form.appendChild(InputIntentId);
            form.appendChild(InputAmount);
            form.appendChild(InputIntentStatus);
            form.appendChild(InputRegistration);
            form.submit();
        }
    }

}