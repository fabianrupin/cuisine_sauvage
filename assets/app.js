import './styles/app.scss';
import $ from 'jquery';
import 'jquery.easing';
import 'bootstrap';
import '@fortawesome/fontawesome-free'

// start the Stimulus application
import './bootstrap';

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});

$('.custom-file-input').on('change', function (event){
    var inputFile = event.currentTarget;
    $(inputFile).parent()
        .find('.custom-file-label')
        .html(inputFile.files[0].name);
})

$('.js-datepicker').datepicker({
    language: 'fr',
    autoclose: true,
    startView: 0

});
