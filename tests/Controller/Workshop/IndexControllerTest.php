<?php

namespace App\Tests\Controller\Workshop;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testCalendarShow(): void
    {
        $client = static::createClient();

        $client->request('GET', '/ateliers/calendrier');

        $this->assertResponseIsSuccessful();
    }
}
