# [CuisineSauvage](https://gitlab.com/fabianrupin/cuisine_sauvage)

[CuisineSauvage](https://gitlab.com/fabianrupin/cuisine_sauvage) is a showcase site of the wild cooking workshop activity 
proposed at La Mettrie (35680 Bais) by Marie Renée Rupin. This application also allows you to register and pay for a 
cooking class : [https://cuisinesauvage.bzh](https://cuisinesauvage.bzh).

## Components and Versions
This project was created on January 2020, and now contains the following components: 
* Symfony 5.4.2, installed with website-full dependencies
* other components :
  * Stripe
  * Twig/inky-extra, for sending emails
  * PhpStan

## Install the project in development environment

To begin using this project, 

* Clone the repo, install back and front dependencies:
```bash
git clone git@gitlab.com:fabianrupin/cuisine_sauvage.git
cd cuisine_sauvage
composer install
yarn install
yarn build
```

* This project comes with a docker file to manage database services, phpmyadmin and an email catcher. To use them, docker 
and docker-compose must be installed. To start these services:
```bash
docker-compose up -d
```

* After that, you just need to create database, migrate the migrations, and load the fixtures:
```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

* By default, the project is in a production environment. To change to the development environment, create a new 
.env.local file:
```bash
echo "APP_ENV=dev" >> .env.local
```

* To customise the environment variables in the production environment, copy the .env.dev file to a .env.dev.local file 
and change your keys there:
```bash
cp .env.dev .env.dev.local
```

Don't forget to clear the cache, and you can launch the local server:
```bash
php bin/console c:c
symfony run -d yarn encore dev --watch
symfony serve --no-tls -d
symfony open:local
```

## Deploy the app in production environment

* install back dependencies, front dependencies and build front files :
```bash
composer install --no-dev --optimize-autoloader
yarn install --force
yarn build
```

* Deploy Secrets to production
```bash
#Update or adding secrets production values
secrets set
#Rotate production decryption key 
secrets:generate-keys --rotate
#Copy the production decryption config/secrets/prod/prod.decrypt.private.php key to your server
```

* Create and migrate the bdd :
```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

* Configure apache configuration
  * In `public` folder copy `.htaccess.dist` to `.htaccess`
  * Comment `Require valid-user` line to remove access with password

Clear the cache:
```bash
php bin/console c:c`
```

## Bugs and Issues

Have a bug or an issue with this project? 
[Open a new issue](https://gitlab.com/fabianrupin/wheelwork/-/issues/new) here on gitlab.

## Creator

CuisineSauvage was created by and is maintained by 
**[Fabian Rupin](https://wheelwork.io/)**, Owner of [WheelWork](https://wheelwork.io/).

## Copyright and License

https://readme.so/fr/editor